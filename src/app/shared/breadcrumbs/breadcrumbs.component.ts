import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivationEnd, Router } from '@angular/router';
import { filter, map, Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss']
})
export class BreadcrumbsComponent implements OnInit, OnDestroy {

  /**
   * Varaibale que almacena el titulo
   * @param router 
   */

  public titulo: string;

  /**
   * Variable que mata la ejecucion
   */

  public tituloSubs$: Subscription;

  constructor(private router: Router) {
    this.titulo = '';
    this.tituloSubs$ = this.getArgumentosRuta()
        .subscribe( ({titulo}) =>{
      this.titulo = titulo;
      document.title = titulo
  
})
   }
  ngOnDestroy(): void {
    this.tituloSubs$.unsubscribe();
  }

  ngOnInit(): void {
  }


  /**
   * Metodo que guarda las rutas
   */

  getArgumentosRuta(){
    return this.router.events
      .pipe(
        filter<any>(event => event instanceof ActivationEnd),
        filter((event: ActivationEnd) => event.snapshot.firstChild === null),
        map((event: ActivationEnd) => event.snapshot.data)
      );
      
  }

}
