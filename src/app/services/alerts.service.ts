import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class AlertsService {

  constructor() { }

  /**
   * Alerta de error
   */

 errorAlert(){
  Swal.fire(
    'Error',
    'Por favor llene los campos correctamente',
    'error'
  )
 }


 
}
