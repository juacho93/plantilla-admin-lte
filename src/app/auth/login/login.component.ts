import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AlertsService } from 'src/app/services/alerts.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  /**
   * Crea el formulario para hacer login
   */
  public login = this.formBuilder.group({
     email: ['', [Validators.required]],
     password: ['', [Validators.required]]
  })

  
  /**
   * Constructor de la clase
   * @param formBuilder 
   * @param alertService
   */
  constructor(private formBuilder: FormBuilder, private alertService: AlertsService) { }

  ngOnInit(): void {
  }

  /**
   * Metodo que hace el logueo de la aplicación
   */

  loginSubmit(){
    if (this.login.invalid) {
      this.login.markAllAsTouched();
      return
    }
  }


  public validateFields(input:string): boolean{
    return this.login.controls[input].invalid && this.login.controls[input].touched
   }
 



}
